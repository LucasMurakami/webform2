﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PanelPage
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNext1_Click(object sender, EventArgs e)
        {
            PanelPersonalInfo.Visible = false;
            PanelAddress.Visible = true;
            PanelLoginArea.Visible = false;
        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            PanelPersonalInfo.Visible = true;
            PanelAddress.Visible = false;
            PanelLoginArea.Visible = false;
        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            PanelPersonalInfo.Visible = false;
            PanelAddress.Visible = false;
            PanelLoginArea.Visible = true;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            PanelPersonalInfo.Visible = false;
            PanelAddress.Visible = true;
            PanelLoginArea.Visible = false;
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            lblConfirmation.Text = "AVISO! Os seus dados foram enviados com sucesso!";
        }
    }
}