﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="PanelPage.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PanelPage</title>

     <script type="text/javascript">
         document.addEventListener("DOMContentLoaded", function () {
             var passwordField = document.getElementById('TxtBSenha');
             var showPasswordCheckbox = document.getElementById('checkBox1');

        showPasswordCheckbox.addEventListener('change', function () {
            passwordField.type = showPasswordCheckbox.checked ? 'text' : 'password';
        });
         });

</script>


</head>
<body style="width: 421px">

    <form id="form1" runat="server">
        <asp:Panel ID="PanelBody" runat="server">

             <table style="width: 100%;" >
            </table>

            <asp:Panel ID="PanelPersonalInfo" runat="server">
                <asp:Label ID="lblPersonalInfo" runat="server" Text="Informações Pessoais" BorderColor="White" BorderWidth="10px"></asp:Label>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100px">Nome: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBNome" runat="server" Width="253px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Sobrenome: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBSobrenome" runat="server" Width="253px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Gênero: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBGenero" runat="server" Width="253px"></asp:TextBox></td>            
                    </tr>
                    <tr>
                        <td style="width: 100px">Celular:</td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBCelular" runat="server" Width="254px"></asp:TextBox></td>            
                    </tr>
                    <tr>
                        <td></td>      
                        <td>
                            <asp:Button ID="btnNext1" runat="server" Text="Proxímo" Width="43%" BackColor="#66FF66" BorderColor="#66FF33" ForeColor="White" OnClick="btnNext1_Click" style="margin-left: 73px" />
                        </td>
                    </tr>
                </table>
                
            </asp:Panel>
            
            <asp:Panel ID="PanelAddress" runat="server" Visible ="false">
                <asp:Label ID="lblAddress" runat="server" Text="Detalhes do endereço" BackColor="White" BorderColor="White" BorderWidth="10px"></asp:Label>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100px">Endereço: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBEndereco" runat="server" Width="241px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Cidade: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBCidade" runat="server" Width="241px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">CEP: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBCEP" runat="server" Width="241px"></asp:TextBox></td>            
                    </tr>           
                    <tr>
                        <td>
                            <asp:Button ID="btnBack1" runat="server" Text="Voltar" Width="86%" BackColor="Red" BorderColor="Red" ForeColor="White" OnClick="btnBack1_Click"/>
                        </td>      
                        <td>
                            <asp:Button ID="btnNext2" runat="server" Text="Próximo" Width="45%" BackColor="#66FF66" BorderColor="#66FF33" ForeColor="White" OnClick="btnNext2_Click" style="margin-left: 69px" />
                        </td>
                    </tr>
                </table>
                
            </asp:Panel>

             <asp:Panel ID="PanelLoginArea" runat="server" Visible ="false">
                 <asp:Label ID="lblLoginArea" runat="server" Text="Área de Login" BackColor="White" BorderColor="White" BorderWidth="10px"></asp:Label>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100px">Usuário: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBUsuario" runat="server" Width="239px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Senha: </td>
                        <td style="width: 100px"><asp:TextBox ID="TxtBSenha" runat="server" TextMode="Password" Width="238px"></asp:TextBox></td>
                    </tr>     
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="checkBox1" runat="server" Text="Show/Hide"/>
                        </td>
                    </tr>
                    <tr>                     
                        <td>
                            <asp:Button ID="btnBack2" runat="server" Text="Voltar" Width="87%" BackColor="Red" BorderColor="Red" ForeColor="White" OnClick="btnBack2_Click"/>
                        </td>      
                        <td>
                            <asp:Button ID="btnSend" runat="server" Text="Enviar" Width="46%" BackColor="#66FF66" BorderColor="#66FF33" ForeColor="White" OnClick="btnSend_Click" style="margin-left: 66px" />
                        </td>
                    </tr>
                   
                </table>
                 <asp:Label runat="server" ID="lblConfirmation" />
                
            </asp:Panel>

        </asp:Panel>
    </form>
</body>
</html>
